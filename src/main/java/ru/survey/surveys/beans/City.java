package ru.survey.surveys.beans;

public class City {
	
	private Integer cityId;
	private String cityName;
	//private Country country;
	
	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	/*
	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	*/

	public City(Integer cityId, String cityName){//, Country country) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
		//this.setCountry(country);
	}

	public City() {

	}
	
	@Override
	public String toString() {
		return "City [cityId=" + cityId + ", cityName=" + cityName
				//+ ", country=" + country + "]";
				+ "]";
	}

}
