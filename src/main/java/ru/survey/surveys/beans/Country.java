package ru.survey.surveys.beans;

import java.util.List;

public class Country {

	private Integer countryCode;
	private String countryName;
	private List<City> cities;
	
	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public Integer getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(Integer countryCode) {
		this.countryCode = countryCode;
	}
	
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Country(Integer countryCode, String countryName) {
		super();
		this.countryCode = countryCode;
		this.countryName = countryName;
	}

	public Country() {

	}

	@Override
	public String toString() {
		return "Country [countryCode=" + countryCode + ", countryName="
				+ countryName + "]";
	}
	
	
	
}
