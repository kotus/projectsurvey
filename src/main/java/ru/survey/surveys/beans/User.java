package ru.survey.surveys.beans;

import java.util.Date;
import java.util.StringJoiner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

//@Entity
//@Table(name = "users")
public class User {

//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "idUsers", length = 11)
    private Integer userID;

//    @Column(name = "user_name")
    private String name;

//    @Column(name = "shure_name")
    private String shureName;

//    @Column(name = "login")
    private String login;

//   @Column(name = "email")
    private String email;

//    @Column(name = "phone")
    private String phone;

//    @Column(name = "password")
    private String password;

    private String password_salt;

//    @Column(name = "create_time")
//    @Temporal(TemporalType.TIMESTAMP)
    private Date createTime;

//    @Column(name = "sex", columnDefinition = "ENUM('MALE','FEMALE')")
    //@Enumerated(EnumType.STRING)
    private UserSex sex;

//    @Column(name = "date_birth")
//    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBirth;

    private Country country;
    private City city;

    public String getPassword_salt() {
        return password_salt;
    }

    public void setPassword_salt(String password_salt) {
        this.password_salt = password_salt;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShureName() {
        return shureName;
    }

    public void setShureName(String shureName) {
        this.shureName = shureName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public UserSex getSex() {
        return sex;
    }

    public void setSex(UserSex sex) {
        this.sex = sex;
    }

    public Date getDateBirth() {
        return dateBirth;
    }

    public void setDateBirth(Date dateBirth) {
        this.dateBirth = dateBirth;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getDateOfCreation() {
        return createTime;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.createTime = dateOfCreation;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public User(Integer userID, String name, String shureName, String email,
                String phone, String password, Date dateBirth) {
        this.userID = userID;
        this.name = name;
        this.shureName = shureName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.dateBirth = dateBirth;
        this.createTime = new Date(System.currentTimeMillis());
    }

    public User(Integer userID, String name, String shureName, String email,
                String phone, String password) {
        this.userID = userID;
        this.name = name;
        this.shureName = shureName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.createTime = new Date(System.currentTimeMillis());
    }

    public User() {
        super();
    }

    public static void main(String[] args) {


    }

    @Override
    public String toString() {
        return this.name + " " + this.shureName + " " + this.email;
    }


}
