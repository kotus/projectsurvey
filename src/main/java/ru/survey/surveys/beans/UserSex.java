package ru.survey.surveys.beans;

public enum UserSex {
	
	MALE, 
	FEMALE;

	UserSex() {
		
	}

	public static UserSex convertToEnglish(String russianOne){

		if (russianOne.equalsIgnoreCase("Мужской")){
			return UserSex.MALE;
		} else {
			return UserSex.FEMALE;
		}

	}

}
