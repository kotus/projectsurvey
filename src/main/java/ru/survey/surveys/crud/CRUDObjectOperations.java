package ru.survey.surveys.crud;

import java.util.List;

/**
 * Created by k.buhantsev on 08.12.2015.
 */
public interface CRUDObjectOperations<T>{

    public T create(T object);

    public boolean delete(T object);

    public T update(T object);

    public T getByID(Integer ID);

    public List<T> getAll();

}
