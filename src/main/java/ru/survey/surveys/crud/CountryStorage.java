package ru.survey.surveys.crud;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import ru.survey.surveys.beans.Country;

import java.util.ArrayList;

/**
 * Created by k.buhantsev on 11.02.2016.
 */
public class CountryStorage implements CRUDObjectOperations<Country>{

    private final SessionFactory factory;

    public CountryStorage(){
        factory = new Configuration().configure().buildSessionFactory();
    }

    @Override
    public Country create(Country object) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.save(object);
            return object;
        } finally {
            tx.commit();
            session.close();
        }
    }

    @Override
    public boolean delete(Country object) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.delete(object);
            return true;
        } finally {
            tx.commit();
            session.close();
        }
    }

    @Override
    public Country update(Country object) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            session.update(object);
            return object;
        } finally {
            tx.commit();
            session.close();
        }
    }

    @Override
    public Country getByID(Integer ID) {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Country as country where country.countryCode=:id");
            query.setParameter("id", ID);
            List result = query.list();
            if (result.isEmpty()){
                return null;
            } else {
                return (Country) result.iterator().next();
            }
        } finally {
            tx.commit();
            session.close();
        }
    }

    @Override
    public List<Country> getAll() {
        final Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        try {
            return session.createQuery("from Country").list();
        } finally {
            tx.commit();
            session.close();
        }
    }

}
