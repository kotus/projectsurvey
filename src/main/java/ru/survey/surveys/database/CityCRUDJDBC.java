package ru.survey.surveys.database;

import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.Country;
import ru.survey.surveys.crud.CRUDObjectOperations;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by k.buhantsev on 11.12.2015.
 */
public class CityCRUDJDBC implements CRUDObjectOperations<City> {

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public CityCRUDJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public City create(City city) {

        String sql = "INSERT INTO city (city_name, fk_country_code) VALUES (?, ?)";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, city.getCityName());
            //ps.setInt(2, city.getCountry().getCountryCode());
            ps.executeUpdate();
            return city;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    @Override
    public boolean delete(City city) {

        String sql = "DELETE FROM city WHERE idCity = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, city.getCityId());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return false;
    }

    @Override
    public City update(City city) {

        String sql = "UPDATE city SET idCity = ?, city_name = ?, fk_country_code = ?" +
                " WHERE idCity = ? ;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, city.getCityId());
            ps.setString(2, city.getCityName());
            //ps.setInt(3, city.getCountry().getCountryCode());
            ps.setInt(4, city.getCityId());
            ps.executeUpdate();
            return city;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    @Override
    public City getByID(Integer ID) {

        String sql = "SELECT city.idCity, city.city_name, country_code, country_name" +
                " FROM city" +
                " LEFT JOIN country ON city.fk_country_code = country.country_code" +
                " WHERE idCity = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, ID);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()){
//                City city = new City(
//                        resultSet.getInt("idCity"), resultSet.getString("city_name"),
//                        new Country(resultSet.getInt("country_code"), resultSet.getString("country_name")));
//                return city;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    public City getByName(String name) {

        String sql = "SELECT city.idCity, city.city_name, country_code, country_name" +
                " FROM city" +
                " LEFT JOIN country ON city.fk_country_code = country.country_code" +
                " WHERE city_name = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()){
                /*
                City city = new City(
                        resultSet.getInt("idCity"), resultSet.getString("city_name"),
                        new Country(resultSet.getInt("country_code"), resultSet.getString("country_name")));
                return city;
                */
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    @Override
    public ArrayList<City> getAll() {

        String sql = "SELECT city.idCity, city.city_name, country_code, country_name" +
                " FROM city" +
                " LEFT JOIN country ON city.fk_country_code = country.country_code;";
        Statement statement = null;
        ArrayList<City> cities = new ArrayList<City>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                /*
                cities.add(new City(resultSet.getInt("idCity"),
                        resultSet.getString("city_name"),
                        new Country(resultSet.getInt("country_code"),
                                resultSet.getString("country_name"))));
                */
            }
            if (!cities.isEmpty()){
                return cities;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;
        
    }

    public ArrayList<City> getCitiesByCountryName(String countryName) {

        String sql = "SELECT city.idCity, city.city_name, country_code, country_name" +
                " FROM city" +
                " LEFT JOIN country ON city.fk_country_code = country.country_code" +
                " WHERE country.country_name= ? ;";
        PreparedStatement ps = null;
        ArrayList<City> cities = new ArrayList<City>();
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, countryName);
            ResultSet resultSet = ps.executeQuery();
            while (resultSet.next()){
                /*
                cities.add(new City(resultSet.getInt("idCity"),
                        resultSet.getString("city_name"),
                        new Country(resultSet.getInt("country_code"),
                                resultSet.getString("country_name"))));
                 */
            }
            if (!cities.isEmpty()){
                return cities;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

}
