package ru.survey.surveys.database;

import ru.survey.surveys.beans.Country;
import ru.survey.surveys.crud.CRUDObjectOperations;

import java.sql.*;
import java.util.ArrayList;

/**
 * Created by k.buhantsev on 10.12.2015.
 */
public class CountryCRUDJDBC implements CRUDObjectOperations<Country> {

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public CountryCRUDJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Country create(Country country) {

        String sql = "INSERT INTO country (country_code, country_name) VALUES (?, ?)";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, country.getCountryCode());
            ps.setString(2, country.getCountryName());
            ps.executeUpdate();
            return country;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;
    }

    @Override
    public boolean delete(Country country) {

        String sql = "DELETE FROM country WHERE country_code=?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, country.getCountryCode());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return false;

    }

    @Override
    public Country update(Country country) {

        String sql = "UPDATE country SET country_code= ?, country_name= ?" +
                " WHERE country_code = ? ;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, country.getCountryCode());
            ps.setString(2, country.getCountryName());
            ps.setInt(3, country.getCountryCode());
            ps.executeUpdate();
            return country;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    @Override
    public Country getByID(Integer ID) {

        String sql = "SELECT country_code, country_name FROM country WHERE country_code = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, ID);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()){
                Country country = new Country(
                        resultSet.getInt("country_code"), resultSet.getString("country_name"));
                return country;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    public Country getByName(String name) {

        String sql = "SELECT country_code, country_name FROM country WHERE country_name = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()){
                Country country = new Country(
                        resultSet.getInt("country_code"), resultSet.getString("country_name"));
                return country;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

    @Override
    public ArrayList<Country> getAll() {

        String sql = "SELECT * FROM country;";
        Statement statement = null;
        ArrayList<Country> countries = new ArrayList<Country>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
                countries.add(
                        new Country(
                                resultSet.getInt("country_code"),
                                resultSet.getString("country_name")));
            }
            if (!countries.isEmpty()){
                return countries;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;

    }

}
