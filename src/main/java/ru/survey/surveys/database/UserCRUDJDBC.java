package ru.survey.surveys.database;

import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.Country;
import ru.survey.surveys.beans.User;
import ru.survey.surveys.beans.UserSex;
import ru.survey.surveys.crud.CRUDObjectOperations;

import java.sql.*;
import java.sql.Date;
import java.util.*;

public class UserCRUDJDBC implements CRUDObjectOperations<User> {

    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public UserCRUDJDBC(Connection connection) {
        this.connection = connection;
    }

    @Override
    public User create(User user) {

        String sql = "INSERT INTO users " +
                " (user_name, shure_name, email, phone, password, login, date_birth, sex, country_code, city_id, password_salt)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, user.getName());
            ps.setString(2, user.getShureName());
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setString(5, user.getPassword());
            ps.setString(6, user.getLogin());
            ps.setDate(7, new Date(user.getDateBirth().getTime()));
            ps.setString(8, user.getSex().name());
            ps.setInt(9, user.getCountry().getCountryCode());
            ps.setInt(10, user.getCity().getCityId());
            ps.setString(11, user.getPassword_salt());
            ps.executeUpdate();

            sql = "SELECT max(idUsers) FROM surveys.users;";
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                user.setUserID(resultSet.getInt(1));
            }

            return user;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return null;
    }

    @Override
    public boolean delete(User user) {

        String sql = "DELETE FROM users WHERE login = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setString(1, user.getLogin());
            ps.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection = null;
        }

        return false;

    }

    @Override
    public User update(User user) {
        return null;
    }

    @Override
    public User getByID(Integer ID) {

        String sql = "SELECT user_name, shure_name, email, phone, password, login, date_birth," +
                " sex, users.country_code, country.country_name, users.city_id, city.city_name" +
                " FROM surveys.users" +
                " LEFT JOIN surveys.city ON users.city_id = city.idCity" +
                " LEFT JOIN surveys.country ON users.country_code = country.country_code" +
                " WHERE users.idUsers = ?;";
        PreparedStatement ps = null;
        try {
            ps = connection.prepareStatement(sql);
            ps.setInt(1, ID);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                User user = new User();
                user.setUserID(ID);
                user.setName(resultSet.getString("user_name"));
                user.setShureName(resultSet.getString("shure_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPhone(resultSet.getString("phone"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setCountry(new Country(resultSet.getInt("country_code"), resultSet.getString("country_name")));
                //user.setCity(new City(resultSet.getInt("city_id"), resultSet.getString("city_name"), user.getCountry()));
                user.setSex(UserSex.valueOf(resultSet.getString("sex")));
                user.setDateBirth(new Date(resultSet.getDate("date_birth").getTime()));
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;

    }

    @Override
    public ArrayList<User> getAll() {

        String sql = "SELECT idUsers, user_name, shure_name, email, phone, password, login, date_birth," +
                " sex, users.country_code, country.country_name, users.city_id, city.city_name" +
                " FROM surveys.users" +
                " LEFT JOIN surveys.city ON users.city_id = city.idCity" +
                " LEFT JOIN surveys.country ON users.country_code = country.country_code;";
        Statement statement = null;
        ArrayList<User> users = new ArrayList<User>();
        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                User user = new User();
                user.setUserID(resultSet.getInt("idUsers"));
                user.setName(resultSet.getString("user_name"));
                user.setShureName(resultSet.getString("shure_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPhone(resultSet.getString("phone"));
                user.setLogin(resultSet.getString("login"));
                user.setPassword(resultSet.getString("password"));
                user.setCountry(new Country(resultSet.getInt("country_code"), resultSet.getString("country_name")));
                //user.setCity(new City(resultSet.getInt("city_id"), resultSet.getString("city_name"), user.getCountry()));
                user.setSex(UserSex.valueOf(resultSet.getString("sex")));
                user.setDateBirth(new Date(resultSet.getDate("date_birth").getTime()));
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
