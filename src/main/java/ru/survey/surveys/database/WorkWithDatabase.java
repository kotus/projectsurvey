package ru.survey.surveys.database;

import org.apache.commons.codec.digest.DigestUtils;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WorkWithDatabase {

    private static String path = "jdbc:mysql://localhost/surveys?";
    private static String userAndPassword = "user=root&password=masterkey";

    /**
     * Метод для создания соединения с базой данных.
     *
     * @return соединение типа java.sql.Connection.
     */
    public static Connection getConnection() {

        Connection conn = null;

        try {

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection(path + userAndPassword);

        } catch (SQLException ex) {
            Logger.getLogger(WorkWithDatabase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex2) {
            Logger.getLogger(WorkWithDatabase.class.getName()).log(Level.SEVERE, null, ex2);
        }

        return conn;

    }

    /**
     * Для отладки данного класса
     *
     * @param args
     */
    public static void main(String[] args) {

		/*
        WorkWithDatabase withDatabase = new WorkWithDatabase();
		String login = withDatabase.checkEmailAndPassword("kotus_88@mail.ru", "70918oe");
		if (login != null) {
			System.out.println(login);
		}
		*/

    }

    /**
     * Метод для проверки данных при авторизации пользователя.
     *
     * @param email    типа String
     * @param enteredPassword типа String
     * @return имя пользователя типа String если найден или NULL если нет.
     */
    public String checkEmailAndPassword(String email, String enteredPassword) {

        Connection connection = getConnection();

        if (connection == null) {
            System.out.println("no connection with DB");
            return null;
        }

        String strSQL = "SELECT * FROM Surveys.users "
                + "where email = ?;";

        PreparedStatement preparedStatement = null;

        try {

            preparedStatement = connection.prepareStatement(strSQL);
            preparedStatement.setString(1, email.trim());
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {

                String usersPasswordHashSumm = resultSet.getString("password");
                String usersPasswordSalt = resultSet.getString("password_salt");

                String enteredHashSumm = DigestUtils.md5Hex(enteredPassword + usersPasswordSalt);

                if (usersPasswordHashSumm.equalsIgnoreCase(enteredHashSumm)) {
                    return resultSet.getString("user_name");
                }

            }

        } catch (SQLException e) {

            e.printStackTrace();

        } finally {

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }

        return null;
    }

}
