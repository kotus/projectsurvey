package ru.survey.surveys.servlets;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ser.std.StdArraySerializers;
import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.Country;
import ru.survey.surveys.database.CityCRUDJDBC;
import ru.survey.surveys.database.WorkWithDatabase;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(value = "/cities/json", name = "CitiesJSON")
public class CitiesJSON extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        String countryName = objectMapper.readValue(request.getInputStream(), String.class);

        CityCRUDJDBC cityJDBC = new CityCRUDJDBC(WorkWithDatabase.getConnection());
        ArrayList<City> cities = new ArrayList<City>();
        cities.addAll(cityJDBC.getCitiesByCountryName(countryName));

        response.addHeader("Content-Type", "application/json; charset=utf-8");
        final ServletOutputStream outputStream = response.getOutputStream();
        objectMapper = new ObjectMapper();
        objectMapper.writeValue(outputStream, cities);
        outputStream.flush();

    }

    // В ГЕТ не передаются серилизованные объекты
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {



    }

}
