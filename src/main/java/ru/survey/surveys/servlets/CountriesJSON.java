package ru.survey.surveys.servlets;

import org.codehaus.jackson.map.ObjectMapper;
import ru.survey.surveys.beans.Country;
import ru.survey.surveys.database.CountryCRUDJDBC;
import ru.survey.surveys.database.WorkWithDatabase;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(value = "/countries/json", name = "CountriesJSON")
public class CountriesJSON extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        CountryCRUDJDBC countryJDBC = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.addAll(countryJDBC.getAll());

        response.addHeader("Content-Type", "application/json; charset=utf-8");
        final ServletOutputStream outputStream = response.getOutputStream();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(outputStream, countries);
        outputStream.flush();

    }

}
