package ru.survey.surveys.servlets.JSON;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;

public class WorkWithJSON {

    public static JSONObject getJSONObject(HttpServletRequest request){

        StringBuffer stringBuffer = makeStringBufferFromRequest(request);

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(stringBuffer.toString());
            return jsonObject;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    public static JSONArray getJSONArray(HttpServletRequest request){

        StringBuffer stringBuffer = makeStringBufferFromRequest(request);

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(stringBuffer.toString());
            return jsonArray;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    // Нужно еще сделать JSONString

    private static StringBuffer makeStringBufferFromRequest(HttpServletRequest request){

        StringBuffer stringBuffer = new StringBuffer();
        String line = null;
        try {
            BufferedReader bufferedReader = new BufferedReader(request.getReader());
            while ((line = bufferedReader.readLine()) != null) {
                stringBuffer.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringBuffer;

    }

}
