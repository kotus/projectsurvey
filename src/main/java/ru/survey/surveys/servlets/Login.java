package ru.survey.surveys.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ru.survey.surveys.database.WorkWithDatabase;

@WebServlet(value = "/login", name = "Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {

        WorkWithDatabase workWithDatabase = new WorkWithDatabase();

        String userName = workWithDatabase.checkEmailAndPassword(request.getParameter("email"), request.getParameter("password"));
        if (userName != null) {

            HttpSession httpSession = request.getSession();
            httpSession.setAttribute("userLogin", userName);

            request.setAttribute("userLogin", userName);
            request.getRequestDispatcher(request.getContextPath()+"/views/surveys.jsp").forward(request, response); //POST

        } else {

            request.setAttribute("userEmail", request.getParameter("email"));
            request.getRequestDispatcher(request.getContextPath()+"/views/loginForm.jsp").forward(request, response);
			/* Так было
			Cookie wrongEmailOrPassword = new Cookie("wrong_data", request.getParameter("email"));
			wrongEmailOrPassword.setMaxAge(1);
			response.addCookie(wrongEmailOrPassword);
			response.sendRedirect(request.getContextPath() + "/views/loginForm.jsp");
			*/

        }

    }

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

        HttpSession httpSession = request.getSession(false);
        if (httpSession.getAttribute("userLogin") != null) {
            response.sendRedirect(request.getContextPath()+"/views/surveys.jsp");
        } else {
            response.sendRedirect(request.getContextPath()+"/views/loginForm.jsp");
        }

	}




	
	

}
