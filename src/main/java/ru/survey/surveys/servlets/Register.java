package ru.survey.surveys.servlets;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONObject;
import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.User;
import ru.survey.surveys.beans.UserSex;
import ru.survey.surveys.database.CityCRUDJDBC;
import ru.survey.surveys.database.UserCRUDJDBC;
import ru.survey.surveys.database.WorkWithDatabase;
import ru.survey.surveys.servlets.JSON.WorkWithJSON;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringJoiner;

@WebServlet(value = "/register", name = "Register")
public class Register extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		User newUser = new User();
		newUser.setName(request.getParameter("userName"));
		newUser.setEmail(request.getParameter("userEmail"));
		newUser.setLogin(request.getParameter("userLogin"));

		String passwordSalt = String.valueOf( (int)(10000+(Math.random()*(90000-10000))) );
		newUser.setPassword(DigestUtils.md5Hex(request.getParameter("userPassword")+passwordSalt));
		newUser.setPassword_salt(passwordSalt);

		newUser.setPhone(request.getParameter("userPhone"));
		newUser.setSex(UserSex.convertToEnglish(request.getParameter("userSex")));
		newUser.setShureName(request.getParameter("userFam"));

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Date date = format.parse(request.getParameter("userDate_birdth"));
			newUser.setDateBirth(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		CityCRUDJDBC cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
		City city = cityCRUD.getByName(request.getParameter("userCity"));

		newUser.setCity(city);
		//newUser.setCountry(city.getCountry());

		UserCRUDJDBC userCRUD = new UserCRUDJDBC(WorkWithDatabase.getConnection());
		if (userCRUD.create(newUser) != null){

			HttpSession httpSession = request.getSession();
			httpSession.setAttribute("userLogin", newUser.getLogin());

			request.setAttribute("userLogin", newUser.getLogin());
			request.getRequestDispatcher(request.getContextPath()+"/views/surveys.jsp").forward(request, response);

		} else {
			System.out.println("Не удалось создать порльзователя");
			response.sendRedirect("http://mail.ru");
		}

	}

}
