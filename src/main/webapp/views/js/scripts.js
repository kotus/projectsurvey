function validate_form(document) {
    
    var valid = true;
    
    var array = ["name", "email", "phone", "password", "password_2"];
    var arrayNames = ["Имя", "Email", "Телефон", "Пароль", "Подтверждение пароля"];
    
    array.forEach(function (item, i, array) {
        if (document.getElementById(item).value == "") {
            document.getElementById(item + "_alert").innerHTML = "Пожалуйста заполните поле '" + arrayNames[i] + "'";
            valid = false;
        } else {
            document.getElementById(item + "_alert").innerHTML = "";
        }
    });
    
    return valid;

}

function validate_form_register(document) {
    
    var valid = true;
    var colorRed = "rgba(246, 120, 154, 0.67)";
    var colorGreen = "rgba(123, 246, 120, 0.56)";
    
    elemName = document.getElementById("name");
    if ( stringText(elemName.value) ) {
        elemName.style.background = colorGreen;
    } else {
        elemName.style.background = colorRed;
        valid = false;
    }
    
    //elemFam = document.getElementById("fam");
    //if ( stringText(elemFam.value) ) {
    //    elemFam.style.background = colorGreen;
    //} else {
    //    elemFam.style.background = colorRed;
    //    valid = false;
    //}
    
    elemCity = document.getElementById("city");
    if ( elemCity.value != '' ) {
        elemCity.style.background = colorGreen;
    } else {
        elemCity.style.background = colorRed;
        valid = false;
    }
    
    elemEmail = document.getElementById("email");
    if ( mail(elemEmail.value) ) {
        elemEmail.style.background = colorGreen;
    } else {
        elemEmail.style.background = colorRed;
        valid = false;
    }
    
    elemLogin = document.getElementById("login");
    if ( login(elemLogin.value) ) {
        elemLogin.style.background = colorGreen;
    } else {
        elemLogin.style.background = colorRed;
        valid = false;
    }
    
    elemPhone = document.getElementById("phone");
    if ( login(elemPhone.value) ) {
        elemPhone.style.background = colorGreen;
    } else {
        elemPhone.style.background = colorRed;
        valid = false;
    }
    
    //
    // date_birdth
    //
    
    elemPassw = document.getElementById("password");
    elemPassw2 = document.getElementById("password_2");
    if (elemPassw.value=="" || elemPassw2.value=="" || elemPassw.value!=elemPassw2.value) {
        alert("Введенные пароли не совпадают!!!");
        valid = false;
        elemPassw.style.background = colorRed;
        elemPassw2.style.background = colorRed;
    } else {
        elemPassw.style.background = colorGreen;
        elemPassw2.style.background = colorGreen;
    }
    
    return valid;
    
}

function validate_form_login(document) {
    
    var valid = true;

    if ( !mail(document.loginForm.email.value) ) {
    	alert("Пожалуйста заполните корректно поле 'Email'");
    	//document.getElementById("name_alert").innerHTML = "Пожалуйста заполните поле 'Имя'";
        //textArea.style.backgroundColor = "red";/
        valid = false;
    } else if (document.loginForm.password.value == "") {
    	alert("Пожалуйста заполните поле 'Пароль'");
    	//document.getElementById("email_alert").innerHTML = "Пожалуйста заполните корректно поле 'Email'";
        valid = false;
    }

    return valid;

}

function text(str) {
    return /[0-9_;:'!~?=+<|>]/g.test(str);
}

function numeric(str) {
    return /^[0-9-\+\(\)\s]+z/.test(str + "z");
}

function mail(str) {
    return /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/.test(str);
}

function login(str) {
    return /^(\S[^@!#$%/^])*[a-zA-Z0-9_-]{3,100}$/.test(str);
}

function stringText(str) {
    return /^(\S[^@!#$%/^])*[a-zA-Zа-яА-Я0-9_-]{3,100}$/.test(str);
}

function stringCity(str) {
    return /^[a-zA-Zа-яА-Я]{2,50}$/.test(str);
}

function phoneNumber(str) {
    return /^[0-9]{1}\([0-9]{3}\)[0-9]{3}-[0-9]{2}\-[0-9]{2}$/.test(str);
}