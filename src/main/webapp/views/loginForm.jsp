<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" session="true"%>

<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>Авторизация</title>

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Bootstrap core CSS -->
<link href="${pageContext.request.contextPath}/views/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/views/navbar-fixed-top.css" rel="stylesheet">

<!-- Мой ява-скрипт для проверки полей формы -->
<script src="${pageContext.request.contextPath}/views/js/scripts.js"></script>

<!-- Мой стиль для формы логина -->
<link href="${pageContext.request.contextPath}/views/css/loginForm.css" rel="stylesheet" type="text/css">

</head>

<body>

	<div class="container">

		<form class="form-signin mg-btm" action="${pageContext.servletContext.contextPath}/login" 
			method="post" name="loginForm" onsubmit="return validate_form_login(document);">
			<h3 class="heading-desc">Логин</h3>

			<c:if test="${userEmail != null}">
				<p class="heading-desc"><font color="red">Неправильный логин или пароль</font></p>				
			</c:if>	
			
			<div class="main">
			
				<label>Email</label>

				<div class="input-group">
					<span class="input-group-addon"><i
							class="glyphicon glyphicon-user"></i></span> <input type="text"
							name="email" class="form-control" placeholder="Email" autofocus id="email"
                                                                                placeholder="Email" autofocus id="email"
							<c:if test="${userEmail != null}"> value = ${userEmail}</c:if> >
				</div>
				<label>Password   <a href="">(забыли пароль)</a></label>

				<div class="input-group">
					<span class="input-group-addon"><i
						class="glyphicon glyphicon-lock"></i></span> <input type="password"
						name="password" class="form-control" placeholder="Password" id="password">
                </div>

				<div class="row">
					<div class="col-xs-6 col-md-6"></div>
					<div class="col-xs-6 col-md-6 pull-right">
						<button type="submit" class="btn btn-large btn-success pull-right">Login</button>
					</div>
				</div>
				

				<span class="clearfix"></span>

				<label><a href="${pageContext.request.contextPath}/views/register.jsp">Зарегистрироваться</a></label>
				
			</div>
		</form>
	</div>

	<!-- Bootstrap core JavaScript
       ============================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/views/js/bootstrap.min.js"></script>
		
</body>

</html>