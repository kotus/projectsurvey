<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%

	HttpSession httpSession = request.getSession(false);
	if (session.getAttribute("userLogin") != null) {       
    	session.invalidate();		
		//session.removeAttribute("userLogin");
    	response.sendRedirect("loginForm.jsp");
	}
	
%>

<!DOCTYPE html">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Logout</title>
</head>
<body>

</body>
</html>