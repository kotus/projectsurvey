<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String userLogin = "";
	HttpSession httpSession = request.getSession(false);
	if (session.getAttribute("userLogin") != null) {
		userLogin = (String) session.getAttribute("userLogin");	
	} else {
		response.sendRedirect("loginForm.jsp");
	}
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>Профиль</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="navbar-fixed-top.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span
					class="glyphicon glyphicon-user"></span> <%= userLogin %> </a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="surveys.jsp">Опросы</a></li>
					<li><a href="#">Оплаты</a></li>
					<li class="active"><a href="#">Профиль</a></li>
				</ul>
			</div>

		</div>

	</div>

	<div class="container">

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron" id="jumbo-опросы">
			<h2>
				<strong>Данные профиля</strong>
			</h2>

			<p>БЛА-БЛА-БЛА</p>

			<div class="col-xs-6 input-group">
				<span class="input-group-addon">@</span> <input type="text"
					class="form-control" placeholder="Фамилия"/>
            </div>
            &nbsp;
            <div class="col-xs-6 input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control"
					placeholder="Имя">
            </div>
            &nbsp;
            <div class="col-xs-6 input-group">
                <span class="input-group-addon">@</span>
                <input type="email" class="form-control"
					placeholder="Email"/>
            </div>
            &nbsp;
            <div class="col-xs-6 input-group">
                <span class="input-group-addon">@</span>
                <input type="text" class="form-control"
					placeholder="Телефон"/>
            </div>
            &nbsp;
			<p>
				<button type="submit" class="btn btn-lg btn-primary" formaction="#"	role="button">
					<span class="glyphicon glyphicon-floppy-saved"> Сохранить</span>
				</button>
			</p>
		</div>
        
        </div>
        <!-- /container -->


        <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        
</body>

</html>