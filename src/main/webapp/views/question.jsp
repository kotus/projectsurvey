<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="jspf/question/question_header.jspf" %>

<%@ include file="jspf/question/question_body.jspf" %>

<%@ include file="jspf/question/question_footer.jspf" %>