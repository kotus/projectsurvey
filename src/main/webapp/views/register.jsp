<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Регистрация нового пользователя</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="css/register.css" rel="stylesheet">

    <!-- Мой ява-скрипт для проверки полей формы -->
    <script src="js/scripts.js"></script>

    <style type="text/css">
        .red {
            color: red;
        }

        .form-signin {
            max-width: 600px;
            display: block;
            background-color: #f7f7f7;
            -moz-box-shadow: 0 0 3px 3px #888;
            -webkit-box-shadow: 0 0 3px 3px #888;
            box-shadow: 0 0 3px 3px #888;
            border-radius: 2px;
            margin-left: auto;
            margin-right: auto;
        }

        .my_table {
            width: 100%;
        }

        .my_table_td {
            padding-left: 20px;
        }
    </style>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            $.ajax({
                url: "/countries/json",
                method: "get",
                dataType: "JSON",
                success: function (data) {
                    var i = 0;
                    var size = data.size;
                    //$('#country').empty();
                    for (i; i != size; ++i) {
                        $('#country').append("<option>" + data[i].countryName + "</option>");
                    }
                }
            });
        });

        function loadCities(){

            $('#city').empty();

            $.ajax({
                url: "/cities/json",
                method: "POST",
                dataType: "JSON",
                data: JSON.stringify($('#country').val()),
                success: function (data) {
                    var i = 0;
                    var size = data.size;
                    $('#city').empty();
                    for (i; i != size; ++i) {
                        $('#city').append("<option>" + data[i].cityName + "</option>");
                    }
                }
            });

        }

        function submitFormRegister(){
            if (!validate_form_register(document)) {
            } else {
                $.ajax({
                    url: '/register/json',
                    method: 'post',
                    dataType: 'JSON',
                    data: JSON.stringify({"name": $('#name').val(), "country": $('#country').val(),
                        "fam": $('#fam').val(), "city": $('#city').val(), "email": $('#email').val(),
                        "login": $('#login').val(), "phone": $('#phone').val(), "password": $('#password').val(),
                        "date_birdth": $('#date_birdth').val(), "password_2": $('#password_2').val(),
                        "sex": $('#sex').val()})
                });
            }
        }

    </script>

</head>
<body>

<!-- Проврека полей ява-скриптом
http://javascript.ru/forum/misc/32283-proverka-zapolneniya-polejj.html
http://ruseller.com/lessons.php?id=592&rub=28
-->

<div class="container">

    <div class="form-signin">

        <form action="${pageContext.servletContext.contextPath}/register" method="post" name="registerForm"
              onsubmit="return validate_form_register(document);">
        <!--<form method="post" name="registerForm" onsubmit="submitFormRegister()">-->
            <h2 class="form-signin-heading">Введите ваши данные</h2>

            <table class="my_table">
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="name">Имя*:</label>
                            <input name="userName" type="text" class="form-control" id="name" placeholder="Имя" value="${name}">
                        </div>
                    </td>
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="country">Страна*:</label>
                            <select name="userCountry" class="form-control" id="country" onchange="loadCities()">
                                <option></option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="fam">Фамилия:</label>
                            <input name="userFam" type="text" class="form-control" id="fam" placeholder="Фамилия" value="${fam}">

                            <p class="red" id="fam_alert"></p>
                        </div>
                    </td>
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="city">Область*:</label>
                            <select class="form-control" id="city" name="userCity">
                                <option></option>
                            </select>
                        </div>
                    </td>
                </tr>
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="email">Email*:</label>
                            <input name="userEmail" type="text" class="form-control" id="email" placeholder="Email" value="${email}">

                            <p class="red" id="email_alert"></p>
                        </div>
                    </td>
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="login">Логин*:</label>
                            <input name="userLogin" type="text" class="form-control" id="login" placeholder="Логин" value="${login}">

                            <p class="red" id="login_alert"></p>
                        </div>
                    </td>
                </tr>
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="phone">Телефон*: x(xxx)xxx-xx-xx</label>
                            <input name="userPhone" type="text" class="form-control" id="phone" placeholder="Телефон" value="${phone}">

                            <p class="red" id="phone_alert"></p>
                        </div>
                    </td>
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="password">Пароль*:</label>
                            <input name="userPassword" type="password" class="form-control" id="password" placeholder="Пароль">

                            <p class="red" id="password_alert"></p>
                        </div>
                    </td>
                </tr>
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="date_birdth">Дата рождения*:</label>
                            <input name="userDate_birdth" type="date" class="form-control" id="date_birdth" value="${date_birdth}">

                            <p class="red" id="date_birdth_alert"></p>
                        </div>
                    </td>
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="password_2">Повторите пароль*:</label>
                            <input name="userPassword_2" type="password" class="form-control" id="password_2" placeholder="Повторите пароль">

                            <p class="red" id="password_2_alert"></p>
                        </div>
                    </td>
                </tr>
                <tr class="my_table_tr">
                    <td class="my_table_td">
                        <div class="form-group">
                            <label for="sex">Пол*:</label>
                            <select name="userSex" class="form-control" id="sex">
                                <option>Мужской</option>
                                <option>Женский</option>
                            </select>

                            <p class="red" id="sex_alert"></p>
                        </div>
                    </td>
                    <td class="my_table_td">
                        <button type="submit" class="btn btn-lg btn-primary btn-block">Зарегистрироваться</button>
                    </td>
                </tr>
            </table>

        </form>

    </div>

</div>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>