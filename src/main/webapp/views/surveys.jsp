<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	String userLogin = "";
	HttpSession httpSession = request.getSession(false);
	if (session.getAttribute("userLogin") != null) {
		userLogin = (String) session.getAttribute("userLogin");	
	} else {
		response.sendRedirect("loginForm.jsp");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>Профиль</title>

<!-- Bootstrap core CSS -->
<link href="${pageContext.request.contextPath}/views/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${pageContext.request.contextPath}/views/navbar-fixed-top.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="${pageContext.request.contextPath}/views/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<!-- Fixed navbar -->
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${pageContext.request.contextPath}/views/logout.jsp" method="post"><span
					class="glyphicon glyphicon-user"></span> <%= userLogin %> </a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="#">Опросы</a></li>
					<li><a href="#">Оплаты</a></li>
					<li><a href="${pageContext.request.contextPath}/views/profile.jsp">Профиль</a></li>
				</ul>
			</div>

		</div>

	</div>

	<div class="container">

		<!-- Main component for a primary marketing message or call to action -->
		<div class="jumbotron" id="jumbo-опросы">

			<p>Contextual classes can be used to color table rows or table
				cells. The classes that can be used are: .active, .success, .info,
				.warning, and .danger.</p>
			<table class="table">
				<thead>
					<tr>
						<th>Компания</th>
						<th>Тема</th>
						<th>Стоимость</th>
						<th>Статус</th>
					</tr>
				</thead>
				<tbody>
					<tr class="success">
						<td><p>Johnson&Johnson</p></td>
						<td>"Важен ли для вас имидж работодателя?"</td>
						<td>5$</td>
						<td>Выполнен</td>
					</tr>
					<tr class="danger">
						<td><p>Mary Kay</p></td>
						<td>овадлыодлоооооооооооооfsdgsfgsdfgsdfgsdfasdfasdfasd</td>
						<td>5$</td>
						<td><button type="button" class="btn btn-primary active">Приступить</button></td>
					</tr>
					<tr class="success">
						<td><p>Яндекс</p></td>
						<td>Dooley</td>
						<td>5$</td>
						<td>Выполнен</td>
					</tr>
				</tbody>
			</table>

		</div>

	</div>
	<!-- /container -->

	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/views/js/bootstrap.min.js"></script>

</body>
</html>