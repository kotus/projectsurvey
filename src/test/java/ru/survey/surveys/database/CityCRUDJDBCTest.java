package ru.survey.surveys.database;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.Country;

import static org.junit.Assert.*;

/**
 * Created by k.buhantsev on 12.12.2015.
 */
public class CityCRUDJDBCTest {

    CityCRUDJDBC cityCRUD = null;
    CountryCRUDJDBC countryCRUD = null;
    Country country = new Country(111, "testCountry");
    //City city = new City(111, "testCity", country);

    @Before
    public void before(){

        countryCRUD = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        //countryCRUD.create(city.getCountry());

    }

    @Test
    public void testAll() throws Exception {

//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertNotNull(cityCRUD.create(city));
//
//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertNotNull(cityCRUD.update(city));
//
//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertNotNull(cityCRUD.getByID(city.getCityId()));
//
//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertNotNull(cityCRUD.getAll());
//
//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertNotNull(cityCRUD.getCitiesByCountryName(city.getCountry().getCountryName()));
//
//        cityCRUD = new CityCRUDJDBC(WorkWithDatabase.getConnection());
//        assertTrue(cityCRUD.delete(city));

    }

    @After
    public void after() {

        countryCRUD = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        countryCRUD.delete(country);

    }

}