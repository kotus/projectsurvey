package ru.survey.surveys.database;

import org.junit.After;
import org.junit.Test;
import ru.survey.surveys.beans.Country;

import java.sql.Connection;

import static org.junit.Assert.*;

public class CountryCRUDJDBCTest {

    Integer countryCode = 111;
    String countryName = "test";
    CountryCRUDJDBC crud = null;

    @Test
    public void testAll() throws Exception {

        Country country = new Country(countryCode, countryName);

        crud = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(crud.create(country));

        crud = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(crud.update(country));

        crud = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(crud.getByID(countryCode));

        crud = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(crud.getAll());

        crud = new CountryCRUDJDBC(WorkWithDatabase.getConnection());
        assertTrue(crud.delete(country));

    }

}