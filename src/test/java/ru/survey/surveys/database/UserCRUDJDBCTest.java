package ru.survey.surveys.database;

import org.junit.Test;
import ru.survey.surveys.beans.City;
import ru.survey.surveys.beans.Country;
import ru.survey.surveys.beans.User;
import ru.survey.surveys.beans.UserSex;

import java.util.Date;

import static org.junit.Assert.*;

public class UserCRUDJDBCTest {

    @Test
    public void testAll() throws Exception {

        User user = new User(0, "test", "shureTest", "test@test.com",
                "99999999999", "tesopassw", new Date(System.currentTimeMillis()));
        user.setLogin("loginTest");
        user.setSex(UserSex.MALE);
        user.setCountry(new Country(804,""));
        //user.setCity(new City(10, "", new Country(804, "")));

        UserCRUDJDBC userCRUD = new UserCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(userCRUD.create(user));

        userCRUD = new UserCRUDJDBC(WorkWithDatabase.getConnection());
        assertNotNull(userCRUD.getByID(user.getUserID()));

        userCRUD = new UserCRUDJDBC(WorkWithDatabase.getConnection());
        assertTrue(!userCRUD.getAll().isEmpty());

        userCRUD = new UserCRUDJDBC(WorkWithDatabase.getConnection());
        assertTrue(userCRUD.delete(user));

    }
}